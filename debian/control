Source: espa-nol
Maintainer: Agustin Martin Domingo <agmartin@debian.org>
Section: text
Priority: optional
Build-Depends: debhelper-compat (= 12),
               quilt
Build-Depends-Indep: ispell,
                     aspell,
                     dictionaries-common-dev (>= 1.23.2),
                     hunspell-tools | myspell-tools | libmyspell-dev (>= 1:3.1-7)
Standards-Version: 4.1.4
Vcs-Browser: https://salsa.debian.org/agmartin/espa-nol
Vcs-Git: https://salsa.debian.org/agmartin/espa-nol.git
Homepage: http://www.datsi.fi.upm.es/~coes

Package: ispanish
Architecture: all
Depends: ${misc:Depends},
         debconf | debconf-2.0,
         ${ispell:Depends}
Suggests: wspanish
Provides: ispell-dictionary
Description: Spanish dictionary for ispell
 This is the Spanish dictionary for use with the ispell spellchecker.
 Put together by Santiago Rodriguez and Jesus Carretero.

Package: myspell-es
Architecture: all
Depends: ${misc:Depends},
         ${hunspell:Depends}
Suggests: hunspell,
          libreoffice-core | openoffice.org-hunspell | openoffice.org-core (>= 2.0.2),
          iceape-browser | iceweasel | icedove
Breaks: openoffice.org (<= 1.0.3-2)
Conflicts: openoffice.org-spellcheck-es,
           hunspell-es
Provides: openoffice.org-spellcheck-es,
          myspell-dictionary,
          myspell-dictionary-es,
          hunspell-dictionary,
          hunspell-dictionary-es,
          hunspell-es
Replaces: openoffice.org-spellcheck-es
Description: Spanish dictionary for myspell
 This is the Spanish dictionary for use with the myspell spellchecker
 which is currently used within OpenOffice.org and the mozilla
 spellchecker. It is based on ispell dictionary put together by
 Santiago Rodriguez and Jesus Carretero.

Package: aspell-es
Architecture: all
Multi-Arch: foreign
Depends: ${misc:Depends},
         ${aspell:Depends}
Provides: aspell-dictionary
Description: Spanish dictionary for aspell
 This is the Spanish dictionary for use with the aspell spellchecker.
 It is based on ispell dictionary put together by
 Santiago Rodriguez and Jesus Carretero.
